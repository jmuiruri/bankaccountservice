HOW TO INSTALL AND CONFIGURE PREREQUISITE DEPENDENCIES

The system requires the following softwares
  * Java JDK 1.8+
  * Maven v3.3


Install Java before installing Maven.
Java can be downloaded from http://www.oracle.com/technetwork/java/javase/downloads/index.html
Apache Maven is available from https://maven.apache.org/download.cgi




CONFIGURE AND PREPARE SOURCE CODE TO BUILD AND RUN PROPERLY

To install the web application runtime dependencies, and test dependencies run the following commands


  mvn package

This will download all the dependencies and install them. It will also build the application.

To run the web application, run the command

  mvn tomcat7:run

The service end points can be tested from the browser from the url

  http://localhost:8080/BankAccount/

If you have an application running on PORT 8080, you can run using the following command

  mvn tomcat7:run -Dmaven.tomcat.port=<port no.>

Enter your preferred port number.

To run back end tests, run the command

  mvn clean test

CODE COVERAGE

The code coverage results can be viewed from target/site/jacoco-ut/index.html
