package co.tala.assessment.controller;

import co.tala.assessment.model.Transaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by kenny on 14/10/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountControllerTests extends AbstractContextControllerTests {

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        this.wac.getServletContext();
    }

    public void resetAccount() {
        FileOutputStream fos;
        try {
            File dataFile = new File(this.wac.getServletContext().getRealPath("/WEB-INF/") + "/account-data.json");
            fos = new FileOutputStream(dataFile);

            // get the content in bytes
            byte[] contentInBytes = "{\"transactions\":[{\"date\":\"2016-10-10T12:00:00.000\",\"amountIn\":100000,\"amountOut\":0}]}".getBytes();

            fos.write(contentInBytes);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }
    }

    @Test
    public void getBalance() throws Exception {
        this.mockMvc.perform(get("/api/1/balance")).andExpect(status().isOk());
    }

    /**
     *  Tests deposit request
     * @throws Exception
     */
    @Test
    public void deposit() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountIn(40000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
    }

    /**
     *  Tests deposit request
     * @throws Exception
     */
    @Test
    public void depositOverMax() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountIn(45000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("2002")));
    }

    /**
     *  Tests deposit frequency
     * @throws Exception
     */
    @Test
    public void depositExceedFrequencyMax() throws Exception {
        this.resetAccount();
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountIn(5000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("2003")));
    }

    /**
     *  Tests deposit limit
     * @throws Exception
     */
    @Test
    public void depositExceedLimit() throws Exception {
        this.resetAccount();
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountIn(40000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/deposit").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("2004")));
    }

    /**
     *  Tests withdraw request
     * @throws Exception
     */
    @Test
    public void withdraw() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountOut(20000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
    }

    /**
     *  Tests withdraw limit exceeded
     * @throws Exception
     */
    @Test
    public void withdrawLimitExceeded() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountOut(21000.0);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("3002")));
    }

    /**
     *  Tests withdraw limit exceeded
     * @throws Exception
     */
    @Test
    public void withdrawFrequencyExceeded() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountOut(5000.0);

        this.resetAccount();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("3003")));
    }

    /**
     *  Tests withdraw limit exceeded
     * @throws Exception
     */
    @Test
    public void withdrawAmountExceeded() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setDate(Calendar.getInstance().getTime());
        transaction.setAmountOut(20000.0);

        this.resetAccount();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        String transactionJSON = mapper.writeValueAsString(transaction);

        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().isOk()).andExpect(jsonPath("$.status", is("OK")));
        this.mockMvc.perform(post("/api/1/withdraw").contentType(MediaType.APPLICATION_JSON).content(transactionJSON.toString())).andExpect(status().is5xxServerError()).andExpect(jsonPath("$.errorCode", is("3005")));
    }

    @After
    public void reset() {
        this.resetAccount();
    }
}
