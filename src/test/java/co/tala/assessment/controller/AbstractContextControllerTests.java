package co.tala.assessment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import javax.servlet.Filter;

/**
 * Created by kenny on 24/07/2016.
 */
@WebAppConfiguration
@ContextConfiguration("classpath:mvc-dispatcher-servlet.xml")
public class AbstractContextControllerTests {

    @Autowired
    protected WebApplicationContext wac;
}
