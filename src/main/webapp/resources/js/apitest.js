/**
 * Created by kenny on 15/10/2016.
 */

checkBalance = function() {
    $.ajax({
        url : "api/1/balance",
        success: function(data) {
            if(data.status == "OK") {
                $("#balanceMessage").html(data.message);
            }
        }
    })
}

deposit = function() {
    var json = {};
    json.date = new Date().toISOString()
    json.amountIn = $("#deposit").val();
    $.ajax({
        url : "api/1/deposit",
        method: "POST",
        headers: {
            "Content-Type" : "application/json"
        },
        data: JSON.stringify(json),
        success: function(data) {
            if(data.status == "OK") {
                $("#depositMessage").html(data.message);
            } else {

            }
        },
        error: function(error) {
            if(error.responseText != null) {
                var err = JSON.parse(error.responseText);
                $("#depositMessage").html(err.errorMessage);
            }
        }
    })
}

withdraw = function() {
    var json = {};
    json.date = new Date().toISOString()
    json.amountOut = $("#withdraw").val();
    $.ajax({
        url : "api/1/withdraw",
        method: "POST",
        headers: {
            "Content-Type" : "application/json"
        },
        data: JSON.stringify(json),
        success: function(data) {
            if(data.status == "OK") {
                $("#withdrawMessage").html(data.message);
            } else {
                $("#withdrawMessage").html(data.errorMessage);
            }
        },
        error: function(error) {
            if(error.responseText != null) {
                var err = JSON.parse(error.responseText);
                $("#withdrawMessage").html(err.errorMessage);
            }
        }
    })
}