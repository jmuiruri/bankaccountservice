<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bank account service test</title>

    <style>
        body {
            background-color: linen;
        }

        .row {
            padding: 5px;
        }

        button {
            padding: 5px;
            width: 100px;
        }
    </style>
    <script src="resources/js/jquery-3.1.1.min.js"></script>
    <script src="resources/js/apitest.js"></script>
</head>
<body>
    <div class="row">
        <button onclick="checkBalance()">Get balance</button>
        <label id="balanceMessage">Your balance is </label>
    </div>

    <div class="row">
        <input type="text" id="deposit"/> <button onclick="deposit()">Deposit</button><br/>
        <label id="depositMessage"> </label>
    </div>

    <div class="row">
        <input type="text" id="withdraw"/> <button onclick="withdraw()">Withdraw</button><br/>
        <label id="withdrawMessage"></label>
    </div>
</body>
</html>