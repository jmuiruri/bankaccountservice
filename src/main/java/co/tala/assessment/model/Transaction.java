package co.tala.assessment.model;

import java.util.Date;

/**
 * Created by kenny on 14/10/2016.
 */
public class Transaction {

    private Double amountIn;

    private Double amountOut;

    private Date date;

    public Double getAmountIn() {
        return amountIn;
    }

    public void setAmountIn(Double amountIn) {
        this.amountIn = amountIn;
    }

    public Double getAmountOut() {
        return amountOut;
    }

    public void setAmountOut(Double amountOut) {
        this.amountOut = amountOut;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
