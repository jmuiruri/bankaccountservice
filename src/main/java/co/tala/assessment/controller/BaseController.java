package co.tala.assessment.controller;

import co.tala.assessment.util.IERROR;
import co.tala.assessment.util.ServiceError;
import co.tala.assessment.util.ServiceException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by kenny on 13/10/2016.
 */
public class BaseController implements IERROR {

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public void handleException(final Exception e, final HttpServletRequest request,
                                HttpServletResponse response) {
        try {
            ServiceError error;
            response.setContentType("application/json");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            if(e instanceof ServiceException) {
                ServiceException serviceException  = (ServiceException)e;
                error = serviceException.getServiceError();
            } else {
                error = ERROR_1000;
            }
            response.getWriter().write(new JSONObject(error).toString());
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally{
            e.printStackTrace();
        }
    }
}
