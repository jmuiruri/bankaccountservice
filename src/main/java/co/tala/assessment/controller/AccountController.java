package co.tala.assessment.controller;

import co.tala.assessment.model.Transaction;
import co.tala.assessment.service.AccountService;
import co.tala.assessment.util.IERROR;
import co.tala.assessment.util.Message;
import co.tala.assessment.util.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by kenny on 13/10/2016.
 */
@Controller
@RequestMapping("/api/1")
public class AccountController extends BaseController {

    private @Autowired
    AccountService accountService;

    private Logger log = LoggerFactory.getLogger(AccountController.class);
    /**
     *
     * @return
     */
    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public @ResponseBody  Message getBalance() {
        Double balance = accountService.getBalance();
        Message m = new Message();
        m.setStatus("OK");
        m.setMessage("Account balance is $" + balance);
        return m;
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public @ResponseBody
    Message deposit(@RequestBody Transaction depositTransaction) {
        try {
            accountService.deposit(depositTransaction);

            Message m = new Message();
            m.setStatus("OK");
            m.setMessage("$" + depositTransaction.getAmountIn() + " deposited to your account. Current balance $" + accountService.getBalance());
            return m;
        } catch (IOException io) {
            log.error("Error depositing", io);
            throw new ServiceException(IERROR.ERROR_1000);
        }

    }

    @RequestMapping(value = "/withdraw", method = RequestMethod.POST)
    public @ResponseBody Message withdraw(@RequestBody Transaction withdrawTransaction) {
        try {
            accountService.withdraw(withdrawTransaction);

            Message m = new Message();
            m.setStatus("OK");
            m.setMessage("withdrawal of $" + withdrawTransaction.getAmountOut() + " successful. Current balance $" + accountService.getBalance());
            return m;
        } catch (IOException io) {
            log.error("Error withdrawing", io);
            throw new ServiceException(IERROR.ERROR_1000);
        }

    }
}
