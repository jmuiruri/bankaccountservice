package co.tala.assessment.dao;

import co.tala.assessment.model.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Repository;

/**
 * Created by kenny on 13/10/2016.
 */
@Repository
public interface AccountDao {

    String getData();

    void writeData(String data);

    boolean addTransaction(Transaction transaction) throws JsonProcessingException;
    
}
