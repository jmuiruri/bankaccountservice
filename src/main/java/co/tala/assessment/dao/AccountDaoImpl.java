package co.tala.assessment.dao;

import co.tala.assessment.model.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.ServletContext;
import java.io.*;

/**
 * Created by kenny on 13/10/2016.
 */
@Repository
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private ServletContext servletContext;

    /**
     *  This function reads the data file and returns a String representation of the data.
     * @return - The account JSON data as a string
     */
    @Override
    public String getData() {
        try {
            File dataFile = new File(servletContext.getRealPath("/WEB-INF/") + "/account-data.json");

            InputStream stream = new FileInputStream(dataFile);
            String data = IOUtils.toString(stream, "UTF-8");
            return data;

        } catch (IOException e) {

        }
        return null;
    }

    /**
     *  This function writes the given string to the file.
     * @param data - The data to be written to the file store.
     */
    @Override
    public void writeData(String data) {
        FileOutputStream fos = null;
        try {
            File dataFile = new File(servletContext.getRealPath("/WEB-INF/") + "/account-data.json");
            fos = new FileOutputStream(dataFile);

            // get the content in bytes
            byte[] contentInBytes = data.getBytes();

            fos.write(contentInBytes);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *  This function adds a {@link Transaction} object to the file data store.
     * @param transaction - The transaction to be saved to the file data store.
     * @return
     * @throws JsonProcessingException - This exception is thrown if there is a problem parsing the JSON data.
     */
    @Override
    public boolean addTransaction(Transaction transaction) throws JsonProcessingException{
        String data = getData();
        JSONObject object = new JSONObject(data);
        JSONArray transactions = object.optJSONArray("transactions");

        ObjectMapper mapper = new ObjectMapper();
        String txString = mapper.writeValueAsString(transaction);
        transactions.put(new JSONObject(txString));
        writeData(object.toString());
        return false;
    }


}
