package co.tala.assessment.util;

/**
 * Created by kenny on 28/07/2016.
 */
public interface IERROR {

    ServiceError ERROR_1000 = new ServiceError("1000","An unhandled exception has occured.");
    ServiceError ERROR_1001 = new ServiceError("1000","An unhandled exception has occured.");
    ServiceError ERROR_1002 = new ServiceError("1000","An unhandled exception has occured.");
    ServiceError ERROR_2001 = new ServiceError("2001","Maximum daily deposit limit has been reached, try again tomorrow");
    ServiceError ERROR_2002 = new ServiceError("2002","Deposit amount is more than the single deposit transaction limit of $40,000.");
    ServiceError ERROR_2003 = new ServiceError("2003","Maximum deposit frequency of 4 transactions has been reached.");
    ServiceError ERROR_2004 = new ServiceError("2004","The deposit amount will exceed the daily limit of $150000.");
    ServiceError ERROR_3001 = new ServiceError("3001","Maximum withdrawal limit of $50,000 has been reached.");
    ServiceError ERROR_3002 = new ServiceError("3002","The maximum you can withdraw per transaction is $20,000.");
    ServiceError ERROR_3003 = new ServiceError("3003","The maximum withdrawal frequency of 3 has been reached.");
    ServiceError ERROR_3004 = new ServiceError("3004","Error, your balance is less than the withdrawal amount.");
    ServiceError ERROR_3005 = new ServiceError("3005","The withdrawal amount will exceed the daily limit of $50000.");
}
