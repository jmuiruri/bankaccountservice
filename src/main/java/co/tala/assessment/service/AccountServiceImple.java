package co.tala.assessment.service;

import co.tala.assessment.dao.AccountDao;
import co.tala.assessment.model.Transaction;
import co.tala.assessment.util.IERROR;
import co.tala.assessment.util.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kenny on 13/10/2016.
 */
@Service
public class AccountServiceImple implements AccountService {

    @Autowired
    private AccountDao accountDao;

    /**
     *  This function gets the balance for the account.
     *  It sums up all the amount-in and amount-out then gets the difference.
     * @return
     */
    @Override
    public Double getBalance() {
        String data = accountDao.getData();
        JSONObject obj = new JSONObject(data);

        JSONArray transactions = obj.optJSONArray("transactions");
        double totalIn = 0.0;
        double totalOut = 0.0;

        for(int i = 0; i < transactions.length(); i++) {
            JSONObject temp = transactions.optJSONObject(i);
            totalIn = totalIn + temp.optDouble("amountIn", 0);
            totalOut = totalOut + temp.optDouble("amountOut", 0);
        }
        return totalIn - totalOut;
    }

    /**
     *  This function processes a deposit transaction.
     * @param depositTransaction - The deposit transaction object
     * @return
     * @throws IOException - Throws an IOException if there is a problem with JSON-Java Object Serialization
     * @throws ServiceException - An appropriate {@link ServiceException} is thrown if there is a problem with the transaction.
     */
    @Override
    public boolean deposit(Transaction depositTransaction) throws IOException, ServiceException{
        if(depositTransaction.getAmountIn() > 40000) {
            throw new ServiceException(IERROR.ERROR_2002);
        }
        String data = accountDao.getData();
        JSONObject obj = new JSONObject(data);
        JSONArray transactions = obj.optJSONArray("transactions");

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date today = Calendar.getInstance().getTime();
        String todayString = format.format(today);

        int frequency = 0;
        double totalDaily = 0.0;

        for(int i = 0; i < transactions.length(); i++) {
            ObjectMapper map = new ObjectMapper();

            JSONObject temp = transactions.optJSONObject(i);
            Transaction tx = map.readValue(temp.toString(), Transaction.class);
            String txDate = format.format(tx.getDate());
            if(txDate.equalsIgnoreCase(todayString) && tx.getAmountIn() != null && tx.getAmountIn() > 0) {
                totalDaily = totalDaily + tx.getAmountIn();
                frequency++;
            }
        }
        if(frequency > 3) {
            throw new ServiceException(IERROR.ERROR_2003);
        } else if(totalDaily >= 150000) {
            throw new ServiceException(IERROR.ERROR_2001);
        } else if((totalDaily + depositTransaction.getAmountIn()) > 150000) {
            throw new ServiceException(IERROR.ERROR_2004);
        }
        return accountDao.addTransaction(depositTransaction);
    }

    /**
     *  This function processes a withdraw transaction.
     * @param withdrawTransaction - The withdraw transaction object.
     * @return
     * @throws IOException - Throws an {@link IOException} if there is a problem with JSON-Java Object Serialization
     * @throws ServiceException - Throws an appropriate {@link ServiceException} if there is a problem with the withdraw transaction.
     */
    @Override
    public boolean withdraw(Transaction withdrawTransaction) throws IOException, ServiceException{
        if(withdrawTransaction.getAmountOut() > 20000) {
            throw new ServiceException(IERROR.ERROR_3002);
        }

        Double balance = getBalance();
        if(balance < withdrawTransaction.getAmountOut()) {
            throw new ServiceException(IERROR.ERROR_3004);
        }

        String data = accountDao.getData();
        JSONObject obj = new JSONObject(data);
        JSONArray transactions = obj.optJSONArray("transactions");

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date today = Calendar.getInstance().getTime();
        String todayString = format.format(today);

        int frequency = 0;
        double totalDaily = 0.0;

        for(int i = 0; i < transactions.length(); i++) {
            ObjectMapper map = new ObjectMapper();
            JSONObject temp = transactions.optJSONObject(i);
            Transaction tx = map.readValue(temp.toString(), Transaction.class);

            String txDate = format.format(tx.getDate());
            if(txDate.equalsIgnoreCase(todayString) && tx.getAmountOut() != null && tx.getAmountOut() > 0) {
                totalDaily = totalDaily + tx.getAmountOut();
                frequency++;
            }
        }

        if(frequency > 2) {
            throw new ServiceException(IERROR.ERROR_3003);
        } else if(totalDaily >= 50000) {
            throw new ServiceException(IERROR.ERROR_3001);
        } else if((totalDaily + withdrawTransaction.getAmountOut()) > 50000) {
            throw new ServiceException(IERROR.ERROR_3005);
        }
        return accountDao.addTransaction(withdrawTransaction);
    }
}
