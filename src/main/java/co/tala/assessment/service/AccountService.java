package co.tala.assessment.service;

import co.tala.assessment.model.Transaction;
import co.tala.assessment.util.ServiceException;

import java.io.IOException;

/**
 * Created by kenny on 13/10/2016.
 */
public interface AccountService {

    Double getBalance();

    boolean deposit(Transaction depositTransaction) throws IOException, ServiceException;

    boolean withdraw(Transaction withdrawTransaction) throws IOException, ServiceException;
}
